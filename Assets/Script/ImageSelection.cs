﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageSelection : MonoBehaviour
{
    public event EventHandler<SpriteSelectedEventArgs> SpriteSelected;

    public void Clicked()
    {
        SpriteSelectedEventArgs args = new SpriteSelectedEventArgs();
        args.ImageSprite = this.GetComponentInParent<Image>().sprite;
        OnSpriteSelected(args);
    }

    protected virtual void OnSpriteSelected(SpriteSelectedEventArgs e)
    {
        EventHandler<SpriteSelectedEventArgs> handler = SpriteSelected;
        if (handler != null)
        {
            handler(this, e);
        }
    }

    public class SpriteSelectedEventArgs : EventArgs
    {
        public Sprite ImageSprite { get; set; }
    }
}
