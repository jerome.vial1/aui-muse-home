﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static ImageSelection;

public class ImageSlider : MonoBehaviour
{

    public GameObject layoutImage;
    public GameObject imageSelectionPrefab;
    public event EventHandler<SpriteSelectedEventArgs> SpriteSelected;

    private List<Sprite> listSprite2D;
    // Start is called before the first frame update
    void Start()
    {
        listSprite2D = loadSprites();

        foreach (Sprite sprite in listSprite2D)
        {
            GameObject imageButton = Instantiate(imageSelectionPrefab, layoutImage.transform);
            imageButton.GetComponent<Image>().sprite = sprite;
            imageButton.GetComponent<Button>().onClick.AddListener(imageButton.GetComponent<ImageSelection>().Clicked);
            imageButton.GetComponent<ImageSelection>().SpriteSelected += c_SpriteSelected;
        }
    }

    private void c_SpriteSelected(object sender, SpriteSelectedEventArgs e)
    {
        EventHandler<SpriteSelectedEventArgs> handler = (EventHandler<SpriteSelectedEventArgs>) SpriteSelected.GetInvocationList()[SpriteSelected.GetInvocationList().Length -1];
        if (handler != null)
        {
            handler(this, e);
            //foreach object un SpriteSelected
            foreach (var n in SpriteSelected.GetInvocationList())
            {
                SpriteSelected -= (EventHandler<SpriteSelectedEventArgs>)n;
            }

        }
    }

    private List<Sprite> loadSprites()
    {
        UnityEngine.Object[] loadedObjects = Resources.LoadAll("Images", typeof(Sprite));
        List<Sprite> listSprites = new List<Sprite>();

        foreach (UnityEngine.Object obj in loadedObjects)
        {
            listSprites.Add((Sprite)obj);
        }

        return listSprites;
    }
}
