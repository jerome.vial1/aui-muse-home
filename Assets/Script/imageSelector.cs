﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using static ImageSelection;


public class imageSelector : MonoBehaviour
{
    public GameObject goImageSlider;

    private ImageSlider imageSlider;

    public int imageIndex = 0;

    private Sprite[] sprites;

    private float initialDistance;

    private Vector3 initialScale;
    // Start is called before the first frame update
    void Start()
    {
        //get images in ressources folder as sprite
        sprites = Resources.LoadAll<Sprite>("Images");
        setImage(imageIndex);
        imageSlider = GameObject.Find("Canvas").GetComponent<ImageSlider>();

        transform.GetChild(0).GetChild(0).GetComponent<Button>().onClick.AddListener(gameObject.GetComponent<imageSelector>().Clicked);


    }

    // Update is called once per frame
    void Update()
    {
                if (Input.touchCount == 2)
        {
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);

            // if any one of touchzero or touchOne is cancelled or maybe ended then do nothing
            if (touchZero.phase == TouchPhase.Ended || touchZero.phase == TouchPhase.Canceled ||
                touchOne.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Canceled)
            {
                return; // basically do nothing
            }

            if (touchZero.phase == TouchPhase.Began || touchOne.phase == TouchPhase.Began)
            {
                initialDistance = Vector2.Distance(touchZero.position, touchOne.position);
                initialScale = transform.localScale;
            }
            else // if touch is moved
            {
                var currentDistance = Vector2.Distance(touchZero.position, touchOne.position);

                //if accidentally touched or pinch movement is very very small
                if (Mathf.Approximately(initialDistance, 0))
                {
                    return; // do nothing if it can be ignored where inital distance is very close to zero
                }

                var factor = currentDistance / initialDistance;
                transform.localScale = initialScale * factor; // scale multiplied by the factor we calculated
            }
        }
    }

    public void Clicked()
    {
        Debug.Log("Clicked");
        imageSlider.SpriteSelected += gameObject.GetComponent<imageSelector>().OnImageSelected;
        //setImage(1);
    }
    public void OnImageSelected(object sender, SpriteSelectedEventArgs e)
    {
        //setImage(2);
        transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = e.ImageSprite;
        imageSlider.SpriteSelected -= gameObject.GetComponent<imageSelector>().OnImageSelected;
    }

    public void setImage(int index)
    {
        imageIndex = index;
         //get child 
        Transform child = transform.GetChild(0).GetChild(0);
        //Set sprite
        child.GetComponent<Image>().sprite = sprites[imageIndex];
    }
}
