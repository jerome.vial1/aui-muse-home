﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

[RequireComponent(typeof(ARTrackedImageManager))]
public class ImageTracking : MonoBehaviour
{
    public GameObject goImageSlider;

    private ImageSlider imageSlider;

    [SerializeField]
    private GameObject[] arObjectsToPlace;

    private ARTrackedImageManager m_TrackedImageManager;

    private Dictionary<string, GameObject> arObjects = new Dictionary<string, GameObject>();

    void Awake()
    {
        m_TrackedImageManager = GetComponent<ARTrackedImageManager>();

        // setup all game objects in dictionary
        foreach (GameObject arObject in arObjectsToPlace)
        {
            GameObject newARObject = Instantiate(arObject, Vector3.zero, Quaternion.identity);
            newARObject.SetActive(false);
            newARObject.name = arObject.name;
            arObjects.Add(arObject.name, newARObject);
        }
    }


    private void Start()
    {
        imageSlider = goImageSlider.GetComponent<ImageSlider>();   
    }

    void OnEnable()
    {
        m_TrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
    }

    void OnDisable()
    {
        m_TrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
    }


    void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
    {
        foreach (ARTrackedImage trackedImage in eventArgs.added)
        {
            UpdateARImage(trackedImage);
        }

        foreach (ARTrackedImage trackedImage in eventArgs.updated)
        {
            UpdateARImage(trackedImage);
        }

        foreach (ARTrackedImage trackedImage in eventArgs.removed)
        {
            Destroy(trackedImage.gameObject);
        }

        CheckOutOfFrameImage(eventArgs.updated);
    }

    private void UpdateARImage(ARTrackedImage trackedImage)
    {
        // Assign and Place Game Object
        AssignGameObject(trackedImage.referenceImage.name, trackedImage.transform.position, trackedImage.transform.rotation, trackedImage.trackingState);
        
        Debug.Log($"trackedImage.referenceImage.name: {trackedImage.referenceImage.name}");
    }

    void AssignGameObject(string name, Vector3 newPosition, Quaternion rotation,UnityEngine.XR.ARSubsystems.TrackingState trackingState)
    {
        if (arObjectsToPlace != null)
        {
            GameObject goARObject = arObjects[name];
            

            if (trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Limited)
            {
                goARObject.SetActive(false);
            }
            else
            {
                goARObject.SetActive(true);
            }

            goARObject.transform.position = newPosition;
            rotation  *= Quaternion.Euler(90, 0, 0);
            goARObject.transform.rotation = rotation;
        }
    }

    private void CheckOutOfFrameImage(List<ARTrackedImage> trackedImages)
    {
        List<string> updatedNames = (List<string>)trackedImages.Select(ti => ti.referenceImage.name);

        foreach (KeyValuePair<string, GameObject> arObject in arObjects)
        {
            if(!updatedNames.Contains(arObject.Key))
            {
               // imageSlider.SpriteSelected -= arObject.Value.GetComponent<imageSelector>().OnImageSelected; 
            }
        }
    }
}